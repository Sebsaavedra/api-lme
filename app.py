import os

import requests
import mysql.connector

from fastapi import FastAPI
from fastapi.responses import FileResponse

app = FastAPI()


# VALIDAR FECHA DE FINIQUITO.
# VIGENCIA DE VINCULACIÓN (fecha_fin >= today() )  ALERTA RETURN. EXCEPTION.
# ESTADO VINCULADO O DESVICULACION

# COD AREA
# SALUD = 4
# COD = 1
# EDU = 3
# JUNJI = 2


@app.get("/liquidacion/{mm}/{aaaa}/{rut}/{area}")
async def make_url(mm, aaaa, rut, area):
    username = 'robot'
    password = 'R3nc4.2022'
    url_login = 'https://redm.cmrenca.cl'
    download_dir = aaaa + "/" + mm
    session = requests.Session()
    payload_login = {'user-id': 'ssaavedra', 'user-pw': '176168390'}
    # LOGIN RED M
    a = session.post(url_login, data=payload_login)
    print("EL RESULTADO DE A ES:", a)
    cookie = session.cookies.get_dict()['PHPSESSID']
    print(cookie)

    state = GetEstadoFuncionario(rut, area)

    #print(state)

    data_funcionario = {'ESTADO' : 'VINCULADO'}


    if state[1] == 'V':


        # ESTABLECE CONTEXTO SEGÚN AREA.
        GetDataRemuneraciones(mm, aaaa, area)
        id_area = GetAreaId(area)
        headers = {
            'Cookie': 'PHPSESSID=932n1vitkihj5vu35bv44pggo1; PHPSESSID=db1133uv661in6gtsid34bncl4'
        }

        # ADM CENTRAL
        if id_area == '921':
            url_print1 = 'https://redm.cmrenca.cl/sistemasEduc/sis_remuCod/informes/inf_liquidacionPrint.php?IdRegistro=' + rut
            d = session.get(url_print1, headers=headers)
            print(d.text)
            with open('./metadata.pdf', 'wb') as f:
                f.write(d.content)
        # JUNJI
        elif id_area == '209':
            url_print2 = 'https://redm.cmrenca.cl/sistemasEduc/sis_remJunji/informes/inf_liquidacionPrint.php?IdRegistro=' + rut
            r = session.get(url_print2, headers=headers)
            print(r.text)
            with open('./metadata.pdf', 'wb') as f2:
                f2.write(r.content)

        # EDUCACION
        elif id_area == '205':
            print("HOLA")
            tipo_educacion = GetTipoEducacion(rut)
            print(tipo_educacion)
            # DOCENTES
            if tipo_educacion == 1:
                url_print_edu1 = 'https://redm.cmrenca.cl/sistemasEduc/sis_remEd2/informes/inf_liquidacionPrint.php?IdRegistro=' + rut
                a1 = session.get(url_print_edu1, headers=headers)
                print(a1)
                with open('./metadata.pdf', 'wb') as fa:
                    print(fa)
                    fa.write(a1.content)

            # NODOCENTES
            elif tipo_educacion == 2:
                url_print_edu2 = 'https://redm.cmrenca.cl/sistemasEduc/sis_remEd2/informes/inf_liquidacionPrintNoDoc.php?IdRegistro=' + rut
                d2 = session.get(url_print_edu2, headers=headers)
                print(d2)
                with open('./metadata.pdf', 'wb') as fe:
                    print(fe)
                    fe.write(d2.content)

            # NODCRITOS
            elif tipo_educacion == 8:
                url_print_edu3 = 'https://redm.cmrenca.cl/sistemasEduc/sis_remEd2/informes/inf_liquidacionPrintDN.php?IdRegistro=' + rut
                d3 = session.get(url_print_edu3, headers=headers)
                print(d3)
                with open('./metadata.pdf', 'wb') as fg:
                    print(fg)
                    fg.write(d3.content)


        # SALUD
        elif id_area == '302':
            url_print4 = 'https://redm.cmrenca.cl/sistemasSalud/sis_remSal/informes/inf_liquidacionPrint.php?IdRegistro=' + rut
            w = session.get(url_print4, headers=headers)
            print(w.text)
            with open('./metadata.pdf', 'wb') as f4:
                f4.write(w.content)
        return FileResponse(path='./metadata.pdf', media_type='application/octet-stream',
                            filename=mm + '_' + aaaa + '_' + rut + '.pdf')
        return {'ESTADO' : 'FUNCIONARIO VINCULADO'}

    else:


        return {"ESTADO": 'FUNCIONARIO DESVINCULADO', "FECHA DESVINCULACIÓN": state[2]}

#FUNCION RETORNA COD ÁREA.
def GetEstadoFuncionario(rut, area):


    if area == '1':
        connection = mysql.connector.connect(database="ren_remCod", user='ssaavedra', password='176168390',
                                             host="35.199.109.191", port=3306)
        cursor_cod = connection.cursor()

        query_state_1 = "SELECT id, estadoFunc, fecha_fin FROM ren_remCod.rem_personal WHERE id=%s"
        cursor_cod.execute(query_state_1, (rut,))
        myresult1 = cursor_cod.fetchall()
        connection.commit()
        for x1 in myresult1:
            return (x1)


    elif area == '2':
        connection = mysql.connector.connect(database="ren_remJunji", user='ssaavedra', password='176168390',
                                             host="35.199.109.191", port=3306)
        cursor_cod2 = connection.cursor()

        query_state_2 = "SELECT id, estadoFunc, fecha_fin FROM ren_remJunji.rem_personal WHERE id=%s"
        cursor_cod2.execute(query_state_2, (rut,))
        myresult2 = cursor_cod2.fetchall()
        connection.commit()
        for x2 in myresult2:
            return (x2)

    elif area == '3':
        connection = mysql.connector.connect(database="ren_remEduc3", user='ssaavedra', password='176168390',
                                             host="35.199.109.191", port=3306)
        cursor_cod3 = connection.cursor()

        query_state_3 = "SELECT id, estadoFunc, fecha_fin FROM ren_remEduc3.rem_personal WHERE id=%s"
        cursor_cod3.execute(query_state_3, (rut,))
        myresult3 = cursor_cod3.fetchall()
        connection.commit()
        for x3 in myresult3:
            return (x3)

    elif area == '4':
        connection = mysql.connector.connect(database="ren_remperSal", user='ssaavedra', password='176168390',
                                             host="35.199.109.191", port=3306)
        cursor_cod4 = connection.cursor()

        query_state_4 = "SELECT id, estadoFunc, fecha_fin FROM ren_remperSal.rem_personal WHERE id=%s"
        cursor_cod4.execute(query_state_4, (rut,))
        myresult4 = cursor_cod4.fetchall()
        connection.commit()
        for x4 in myresult4:
            return (x4)





#FUNCIÓN RETORNA EL TIPO DE PERSONAL EN ÁREA = EDUCACIÓN.
def GetTipoEducacion(rut):
    connection = mysql.connector.connect(database="ren_remEduc3", user='ssaavedra', password='176168390',
                                         host="35.199.109.191", port=3306)
    cursor_educacion = connection.cursor()
    query1 = sql_context1 = "SELECT id, tipo_fun FROM ren_remEduc3.rem_personal WHERE id=%s"
    cursor_educacion.execute(sql_context1, (rut,))
    myresult = cursor_educacion.fetchall()
    connection.commit()
    for x in myresult:
        return x[1]


#FUNCION RETORNA COD ÁREA.
def GetAreaId(area):
    if area == '1':
        return '921'
    elif area == '2':
        return '209'
    elif area == '3':
        return '205'
    elif area == '4':
        return '302'


#FUNCIÓN ESTABLECE MES Y AÑO PARA LA LIQUIDACIÓN.
def GetDataRemuneraciones(mm, aaaa, area):

    fecha_contexto = GetDataRemProceso(aaaa,mm, area)

    if fecha_contexto == True:
        connection = mysql.connector.connect(database="ren_@admin", user='ssaavedra', password='176168390',
                                             host="35.199.109.191", port=3306)
        cursor = connection.cursor()
        # ESTABLECE CONTEXTO SEGÚN AREA.
        if area == '1':
            query1 = sql_context1 = "UPDATE adm_historicosActivos set mmCodEd= '00', aaaaCodEd= '0000' where id = 'ssaavedra'"
            cursor.execute(sql_context1,)
            connection.commit()
        elif area == '2':
            query2 = sql_context2 = "UPDATE adm_historicosActivos set mmJunji= '00', aaaaJunji= '0000' where id = 'ssaavedra'"
            cursor.execute(sql_context2,)
            connection.commit()
        elif area == '3':
            query3 = sql_context3 = "UPDATE adm_historicosActivos set mmEduc= '00', aaaaEduc= '0000' where id = 'ssaavedra'"
            cursor.execute(sql_context3, )
            connection.commit()
        elif area == '4':
            query4 = sql_context4 = "UPDATE adm_historicosActivos set mmSalud= '00', aaaaSalud= '0000' where id = 'ssaavedra'"
            cursor.execute(sql_context4, )
            connection.commit()
        print(cursor.rowcount, "record(s) affected")

    else:


        connection = mysql.connector.connect(database="ren_@admin", user='ssaavedra', password='176168390',
                                             host="35.199.109.191", port=3306)
        cursor = connection.cursor()
        # ESTABLECE CONTEXTO SEGÚN AREA.
        if area == '1':
            query1 = sql_context1 = "UPDATE adm_historicosActivos set mmCodEd= %s, aaaaCodEd= %s where id = 'ssaavedra'"
            cursor.execute(sql_context1, (mm, aaaa,))
            connection.commit()
        elif area == '2':
            query2 = sql_context2 = "UPDATE adm_historicosActivos set mmJunji= %s, aaaaJunji= %s where id = 'ssaavedra'"
            cursor.execute(sql_context2, (mm, aaaa,))
            connection.commit()
        elif area == '3':
            query3 = sql_context3 = "UPDATE adm_historicosActivos set mmEduc= %s, aaaaEduc= %s where id = 'ssaavedra'"
            cursor.execute(sql_context3, (mm, aaaa,))
            connection.commit()
        elif area == '4':
            query4 = sql_context4 = "UPDATE adm_historicosActivos set mmSalud= %s, aaaaSalud= %s where id = 'ssaavedra'"
            cursor.execute(sql_context4, (mm, aaaa,))
            connection.commit()
        print(cursor.rowcount, "record(s) affected")



#FUNCIÓN RESUELVE SI ESTA EN MES ACTIVO.
def GetDataRemProceso(aaaa, mm, area):
    connection = mysql.connector.connect(database="ren_remCod", user='ssaavedra', password='176168390',
                                         host="35.199.109.191", port=3306)
    cursor = connection.cursor()
    # ESTABLECE CONTEXTO SEGÚN AREA.
    if area == '1':
        query1 = sql_context1 = "SELECT idproc FROM ren_remCod.rem_proceso"
        cursor.execute(sql_context1, ())
        myresult = cursor.fetchall()
        connection.commit()
        for x in myresult:
            txt = str(x[0])
            anio = txt[:4]
            mes = txt.split(anio)
            print(anio)
            print(mes[1])
            date_remProceso = anio+mes[1]
            print(date_remProceso)
            date_query = aaaa+mm
            print(date_query)

            if date_query == date_remProceso:
               return True
            else:
               return False

    elif area == '2':
        query2 = sql_context2 = "SELECT idproc FROM ren_remJunji.rem_proceso"
        cursor.execute(sql_context2, ())
        myresult2 = cursor.fetchall()
        connection.commit()
        for x in myresult2:
            txt = str(x[0])
            anio = txt[:4]
            mes = txt.split(anio)
            print(anio)
            print(mes[1])
            date_remProceso = anio + mes[1]
            print(date_remProceso)
            date_query = aaaa + mm
            print(date_query)

            if date_query == date_remProceso:
                return True
            else:
                return False

    elif area == '3':
        query3 = sql_context3 = "SELECT idproc FROM ren_remEduc3.rem_proceso"
        cursor.execute(sql_context3, ())
        myresult3 = cursor.fetchall()
        connection.commit()
        for x in myresult3:
            txt = str(x[0])
            anio = txt[:4]
            mes = txt.split(anio)
            print(anio)
            print(mes[1])
            date_remProceso = anio + mes[1]
            print(date_remProceso)
            date_query = aaaa + mm
            print(date_query)

            if date_query == date_remProceso:
                return True
            else:
                return False

    elif area == '4':
        query4 = sql_context4 = "SELECT idproc FROM ren_remperSal.rem_proceso"
        cursor.execute(sql_context4, ())
        myresult4 = cursor.fetchall()
        connection.commit()
        for x in myresult4:
            txt = str(x[0])
            anio = txt[:4]
            mes = txt.split(anio)
            print(anio)
            print(mes[1])
            date_remProceso = anio + mes[1]
            print(date_remProceso)
            date_query = aaaa + mm
            print(date_query)

            if date_query == date_remProceso:
                return True
            else:
                return False



@app.get("/hola")
async def main():
    file_name = "ejemplo_1.pdf"
    # DEPENDS ON WHERE YOUR FILE LOCATES
    file_path = os.getcwd() + "/" + file_name
    return FileResponse(path=file_path, media_type='application/octet-stream', filename=file_name)
